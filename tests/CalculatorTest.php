<?php

use App\Services\AverageOperator;
use App\Services\Calculator;
use App\Services\MaxDateOperator;
use App\Services\MaxOperator;
use App\Services\MedianOperator;
use App\Services\MinDateOperator;
use App\Services\MinOperator;
use App\Services\UnderPerformingDatesOperator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CalculatorTest extends KernelTestCase
{
    private array $data;

    public function __construct()
    {
        $this->data = [
            ["metricValue" => '101.25', "dtime" => "2018-01-29"],
            ["metricValue" => '104.08', "dtime" => "2018-02-27"],
            ["metricValue" => '102.93', "dtime" => "2018-02-28"],
            ["metricValue" => '111.00', "dtime" => "2018-01-22"],
            ["metricValue" => '121.00', "dtime" => "2018-03-22"],
        ];
        parent::__construct();
    }

    public function test_calculate_min_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MinOperator());
        $this->assertSame(101.25, $calculator->calculate($this->data));
    }

    public function test_calculate_max_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MaxOperator());
        $this->assertSame(121.00, $calculator->calculate($this->data));
    }

    public function test_calculate_median_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MedianOperator());
        $this->assertSame(104.08, $calculator->calculate($this->data));
    }

    public function test_calculate_average_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new AverageOperator());
        $this->assertSame(108.052, $calculator->calculate($this->data));
    }

    public function test_calculate_min_date_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MinDateOperator());
        $this->assertSame('2018-01-22', $calculator->calculate($this->data));
    }

    public function test_calculate_max_date_value(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MaxDateOperator());
        $this->assertSame('2018-03-22', $calculator->calculate($this->data));
    }

    public function test_get_under_performing_dates(): void
    {
        $calculator = new Calculator();
        $calculator->setOperation(new UnderPerformingDatesOperator());
        $this->assertSame("2018-01-29, 2018-02-27, 2018-02-28", $calculator->calculate($this->data));
    }
}
