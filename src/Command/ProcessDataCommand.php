<?php

namespace App\Command;

use App\Services\AverageOperator;
use App\Services\Calculator;
use App\Services\MaxDateOperator;
use App\Services\MaxOperator;
use App\Services\MedianOperator;
use App\Services\MinDateOperator;
use App\Services\MinOperator;
use App\Services\UnderPerformingDatesOperator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProcessDataCommand extends Command
{
    protected static $defaultName = 'app:process-data';
    private string $inputDirectory = 'public/inputs/';
    private string $outputDirectory = 'public/outputs/';

    protected function configure(): void
    {
        $this->setDescription('Generate output files with min, max, median and under-performing data ');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $finder = new Finder();
        $finder->files()->in($this->inputDirectory);
        if (!$finder->hasResults()) {
            throw new FileNotFoundException(sprintf('No files found in directory "%s"', $this->inputDirectory));
        }

        foreach ($finder as $file) {
            $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
            $data = $serializer->decode($file->getContents(), 'json');
            $this->saveToFile($file, $data);
        }
        return Command::SUCCESS;
    }

    /**
     * Get the first date from the dataset
     * @param array $data
     * @return string
     */
    private function getPeriodFrom(array $data): string
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MinDateOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get the last date from the dataset
     * @param array $data
     * @return string
     */
    private function getPeriodTo(array $data): string
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MaxDateOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get average from data
     * @param array $data
     * @return float
     */
    private function getAverage(array $data): float
    {
        $calculator = new Calculator();
        $calculator->setOperation(new AverageOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get min value of data
     * @param array $data
     * @return float
     */
    private function getMin(array $data): float
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MinOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get max value
     * @param array $data
     * @return float
     */
    private function getMax(array $data): float
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MaxOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get median calculation
     * @param array $data
     * @return float
     */
    private function getMedian(array $data): float
    {
        $calculator = new Calculator();
        $calculator->setOperation(new MedianOperator());
        return $calculator->calculate($data);
    }

    /**
     * Get under-performing dates
     * @param array $data
     * @return string
     */
    private function getUnderPerformingDates(array $data): string
    {
        $calculator = new Calculator();
        $calculator->setOperation(new UnderPerformingDatesOperator());
        return $calculator->calculate($data);
    }

    /**
     * Create file and save the content to the file
     * @param $file
     * @param $data
     */
    private function saveToFile($file, $data): void
    {
        $filesystem = new Filesystem();
        $outputFileName = $this->outputDirectory . $file->getFilenameWithoutExtension() . '.output';

        // If the file already exists remove it
        if ($filesystem->exists($outputFileName)) {
            $filesystem->remove($outputFileName);
        }

        try {
            $filesystem->touch($outputFileName);
            $filesystem->appendToFile($outputFileName, $this->formatContent($data));
        } catch (IOExceptionInterface $exception) {
            echo "An error occurred while creating your file at " . $exception->getPath() . $exception->getMessage();
        }
    }

    /**
     * Format message to be exported
     * @param array $data
     * @return string
     */
    private function formatContent(array $data): string
    {
        $content = "SamKnows Metric Analyser v1.0.0" . PHP_EOL;
        $content .= "===============================" . PHP_EOL . PHP_EOL;
        $content .= "Period checked: " . PHP_EOL . PHP_EOL;
        $content .= "   From:   " . $this->getPeriodFrom($data) . PHP_EOL;
        $content .= "   To:     " . $this->getPeriodTo($data) . PHP_EOL . PHP_EOL;
        $content .= "Statistic: " . PHP_EOL . PHP_EOL;
        $content .= "   Unit: Megabits per second" . PHP_EOL . PHP_EOL;
        $content .= "   Average: " . $this->getAverage($data) . PHP_EOL;
        $content .= "   Min: " . $this->getMin($data) . PHP_EOL;
        $content .= "   Max: " . $this->getMax($data) . PHP_EOL;
        $content .= "   Median: " . $this->getMedian($data) . PHP_EOL . PHP_EOL;
        $content .= "Under-performing periods:" . PHP_EOL . PHP_EOL;
        $content .= "   * The following period(s) were below average: " . $this->getUnderPerformingDates($data) . PHP_EOL;
        return $content;
    }
}
