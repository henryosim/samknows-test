<?php

namespace App\Services;

class MaxDateOperator implements OperatorInterface
{
    /**
     * Calculate maximum date range
     * @param array $input
     * @return string
     */
    public function calculate(array $input): string
    {
        $maxDate = null;
        foreach ($input as $item) {
            $date = $item['dtime'];
            if ($date > $maxDate || is_null($maxDate)) {
                $maxDate = $date;
            }
        }
        return $maxDate;
    }
}
