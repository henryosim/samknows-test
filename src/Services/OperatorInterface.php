<?php

namespace App\Services;

interface OperatorInterface
{
    /**
     * Perform calculation on data
     * @param array $input
     * @return float|string
     */
    public function calculate(array $input): float|string;
}
