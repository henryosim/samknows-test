<?php

namespace App\Services;

class Calculator
{
    /** @var OperatorInterface */
    protected OperatorInterface $operation;

    /** @var array */
    protected array $result;

    /**
     * Set the operation that needs to be calculated
     * @param mixed $operator
     */
    public function setOperation(OperatorInterface $operator): void
    {
        $this->operation = $operator;
    }

    /**
     * Perform calculation based on selected operator
     * @param array $data
     * @return float|string
     */
    public function calculate(array $data): float|string
    {
        return $this->operation->calculate($data);
    }


}
