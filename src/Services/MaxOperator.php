<?php

namespace App\Services;

class MaxOperator implements OperatorInterface
{
    /**
     * Calculate Minimum value
     * @param array $input
     * @return float
     */
    public function calculate(array $input): float
    {
        $maxValue = null;
        foreach ($input as $item) {
            $value = $item['metricValue'];
            if ($value > $maxValue || is_null($maxValue)) {
                $maxValue = $value;
            }
        }
        return $maxValue;
    }
}
