<?php

namespace App\Services;

class UnderPerformingDatesOperator implements OperatorInterface
{
    /**
     * calculate under-performing dates
     * @param array $input
     * @return string
     */
    public function calculate(array $input): string
    {
        $averageOperator = new AverageOperator();
        $average = $averageOperator->calculate($input);
        $dates = [];
        foreach ($input as $item) {
            $value = $item['metricValue'];
            if ($value < $average) {
                $dates[] = $item['dtime'];
            }
        }
        return implode(', ', $dates);
    }
}
