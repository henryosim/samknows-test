<?php

namespace App\Services;

class MedianOperator implements OperatorInterface
{
    /**
     * Calculate Median value
     * @param array $input
     * @return float
     */
    public function calculate(array $input): float
    {
        $values = array_column($input, 'metricValue');
        sort($values);
        $count = count($values);
        $middleVal = floor(($count - 1) / 2);
        if ($count % 2) {
            $median = $values[$middleVal];
        } else {
            $low = $values[$middleVal];
            $high = $values[$middleVal + 1];
            $median = (($low + $high) / 2);
        }
        return $median;
    }
}
