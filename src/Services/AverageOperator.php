<?php

namespace App\Services;

class AverageOperator implements OperatorInterface
{
    /**
     * Calculate average value for data-set
     * @param array $input
     * @return float
     */
    public function calculate(array $input): float
    {
        $values = array_column($input, 'metricValue');
        return array_sum($values) / count($values);
    }
}
