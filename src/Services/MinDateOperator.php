<?php

namespace App\Services;

class MinDateOperator implements OperatorInterface
{
    /**
     * Calculate minimum date range
     * @param array $input
     * @return string
     */
    public function calculate(array $input): string
    {
        $minDate = null;
        foreach ($input as $item) {
            $date = $item['dtime'];
            if ($date < $minDate || is_null($minDate)) {
                $minDate = $date;
            }
        }
        return $minDate;
    }
}
