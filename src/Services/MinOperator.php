<?php

namespace App\Services;

class MinOperator implements OperatorInterface
{
    /**
     * Calculate Minimum value
     * @param array $input
     * @return float
     */
    public function calculate(array $input): float
    {
        $minValue = null;
        foreach ($input as $item) {
            $value = $item['metricValue'];
            if ($value < $minValue || is_null($minValue)) {
                $minValue = $value;
            }
        }
        return $minValue;
    }
}
