# Symfony Docker

This is a symfony 5 project built with [symfony-docker template](https://github.com/dunglas/symfony-docker)

## Getting Started

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Run `docker-compose build --pull --no-cache` to build fresh images
3. Run `docker-compose up -d` (the logs will be displayed in the current shell)
4. Run `docker-compose exec php bin/console app:process-data` (to process the files in the public/input to public/output
   directories)
5. Run `docker-compose exec php bin/phpunit` (to run phpunit tests)

**Kind regards!**
